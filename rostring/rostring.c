/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rostring.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: exam <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/05 10:54:29 by exam              #+#    #+#             */
/*   Updated: 2017/12/05 11:38:22 by exam             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <unistd.h>

int		ft_strlen(char *str)
{
	int i;

	i = 0;
	while (str[i])
		i++;
	return (i);
}

int		ft_word_count(char *str)
{
	int	result;
	int	i;
	int len;

	i = 1;
	len = ft_strlen(str);
	result = 0;
	if (str[0] != ' ' && str[0] != '\t')
		result++;
	while (i < len)
	{
		if (str[i] != ' ' && str[i] != '\t' &&
				(str[i - 1] == ' ' || str[i - 1] == '\t'))
			result++;
		i++;
	}
	return (result);
}

char	**get_wordtab(int words_amount, char *str)
{
	int		i;
	int		j;
	char	**wordtab;

	if ((wordtab = malloc(words_amount * sizeof(char*))) == 0)
		return (0);
	i = 0;
	j = 0;
	if (str[0] != ' ' && str[0] != '\t')
		wordtab[j++] = str;
	while (str[++i])
	{
		if (str[i] != ' ' && str[i] != '\t' &&
				(str[i - 1] == ' ' || str[i - 1] == '\t' || !str[i - 1]))
			wordtab[j++] = str + i;
		if (str[i] == ' ' || str[i] == '\t')
			str[i] = 0;
	}
	return (wordtab);
}

int		main(int argc, char **argv)
{
	int		words_amount;
	char	**wordtab;
	int		i;

	if (argc >= 2)
	{
		words_amount = ft_word_count(argv[1]);
		if ((wordtab = get_wordtab(words_amount, argv[1])) == 0)
			return (0);
		i = 0;
		while (++i < words_amount)
		{
			write(1, wordtab[i], ft_strlen(wordtab[i]));
			write(1, " ", 1);
		}
		write(1, wordtab[0], ft_strlen(wordtab[0]));
		free(wordtab);
	}
	write(1, "\n", 1);
	return (0);
}

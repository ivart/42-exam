/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   computer.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: exam <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/16 10:35:17 by exam              #+#    #+#             */
/*   Updated: 2018/01/16 10:35:26 by exam             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ops.h"

int	compute(int a, int b, char op)
{
	if (op == '+')
		return (sum(a, b));
	else if (op == '-')
		return (min(a, b));
	else if (op == '*')
		return (mult(a, b));
	else if (op == '/')
		return (div(a, b));
	return (mod(a, b));
}

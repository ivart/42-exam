/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   utils.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: exam <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/16 10:35:46 by exam              #+#    #+#             */
/*   Updated: 2018/01/16 10:35:48 by exam             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>

/*
** Goes until " " and checks if all charachters are digits
** or leading -
*/

_Bool	is_number(char *a)
{
	if (*a == '-')
		a++;
	while (*a <= '9' && *a >= '0')
		a++;
	if (*(a - 1) == '-')
		return (0);
	if (!*a || *a == ' ')
		return (1);
	return (0);
}

_Bool	is_operator(char *a)
{
	if (*a == '+' || *a == '-' || *a == '*' ||
			*a == '/' || *a == '%')
		return (1);
	return (0);
}

int		error(void)
{
	printf("Error\n");
	return (0);
}

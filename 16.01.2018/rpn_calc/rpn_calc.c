/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rpn_calc.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: exam <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/16 10:35:40 by exam              #+#    #+#             */
/*   Updated: 2018/01/16 10:42:15 by exam             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include <stdlib.h>
#include "rpn_calc.h"

int		add_number(t_list *numbers, int n)
{
	while (numbers->next)
		numbers = numbers->next;
	if ((numbers->next = malloc(sizeof(t_list))) == 0)
		return (0);
	(numbers->next)->next = NULL;
	(numbers->next)->n = n;
	return (1);
}

t_list	*get_a_n_b(t_list *numbers)
{
	if (numbers->next == NULL)
		return (NULL);
	while ((numbers->next)->next)
		numbers = numbers->next;
	return (numbers);
}

void	move_argv(char **a)
{
	while (**a && **a != ' ')
		(*a)++;
	if (**a)
		(*a)++;
}

int		while_loop(char *str, t_list *numbers)
{
	t_list	*tmp;

	while (*str)
	{
		if (is_number(str))
		{
			if (add_number(numbers, atoi(str)) == 0)
				return (0);
		}
		else if (is_operator(str))
		{
			if ((tmp = get_a_n_b(numbers)) == NULL)
				return (0);
			if (*str == '/' && (tmp->next)->n == 0)
				return (0);
			tmp->n = compute(tmp->n, (tmp->next)->n, *str);
			free(tmp->next);
			tmp->next = NULL;
		}
		else
			return (0);
		move_argv(&str);
	}
	return (1);
}

int		main(int argc, char **argv)
{
	t_list	*numbers;

	if (argc != 2)
		return (error());
	if ((numbers = malloc(sizeof(t_list))) == 0)
		return (error());
	numbers->next = NULL;
	if (!is_number(argv[1]))
		return (error());
	numbers->n = atoi(argv[1]);
	move_argv(&(argv[1]));
	if (!while_loop(argv[1], numbers) || numbers->next)
		return (error());
	printf("%d\n", numbers->n);
	return (0);
}

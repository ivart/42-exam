/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rpn_calc.h                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: exam <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/26 11:02:20 by exam              #+#    #+#             */
/*   Updated: 2017/12/26 12:21:40 by exam             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef RPN_CALC_H
# define RPN_CALC_H

typedef	struct	s_list
{
	struct s_list	*next;
	int				n;
}				t_list;
int				compute(int a, int b, char op);
int				error(void);
_Bool			is_number(char *a);
_Bool			is_operator(char *a);
#endif

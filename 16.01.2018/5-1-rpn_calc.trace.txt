= Host-specific information ====================================================
$> hostname; uname -msr
e3r5p8.42.fr
Darwin 16.7.0 x86_64
$> date
Tue Jan 16 10:44:00 CET 2018
$> gcc --version
Configured with: --prefix=/Applications/Xcode.app/Contents/Developer/usr --with-gxx-include-dir=/usr/include/c++/4.2.1
Apple LLVM version 9.0.0 (clang-900.0.38)
Target: x86_64-apple-darwin16.7.0
Thread model: posix
InstalledDir: /Applications/Xcode.app/Contents/Developer/Toolchains/XcodeDefault.xctoolchain/usr/bin
$> clang --version
Apple LLVM version 9.0.0 (clang-900.0.38)
Target: x86_64-apple-darwin16.7.0
Thread model: posix
InstalledDir: /Applications/Xcode.app/Contents/Developer/Toolchains/XcodeDefault.xctoolchain/usr/bin

= User files collection ========================================================
Collecting user files from Vogsphere
Repository URL: auto-exam/2018/c-exam-alone-in-the-dark-beginner/exam_20180116/aivanov

= Git history ==================================================================
$> git -C /var/folders/sm/47wyt1p97rb0btz8gd0syt8r0000gq/T/tmpm5pXPI/user log --pretty='%H - %an, %ad : %s'
57215e8c31da003ba6f0fb2eda7a2dced20c0bf6 - Exam 42, Tue Jan 16 10:42:47 2018 +0100 : rpn
27db146e81b489350084aed6a04049dab3718c39 - Exam 42, Tue Jan 16 10:28:21 2018 +0100 : ft_itoa_base
 
= Collected files ==========================================
$> ls -lAR /var/folders/sm/47wyt1p97rb0btz8gd0syt8r0000gq/T/tmpm5pXPI/user
total 8
-rw-r--r--  1 deepthought  deepthought  740 Jan 16 10:44 __GIT_HISTORY
drwxr-xr-x  3 deepthought  deepthought  102 Jan 16 10:44 ft_itoa_base
drwxr-xr-x  8 deepthought  deepthought  272 Jan 16 10:44 rpn_calc

/var/folders/sm/47wyt1p97rb0btz8gd0syt8r0000gq/T/tmpm5pXPI/user/ft_itoa_base:
total 8
-rw-r--r--  1 deepthought  deepthought  1817 Jan 16 10:44 ft_itoa_base.c

/var/folders/sm/47wyt1p97rb0btz8gd0syt8r0000gq/T/tmpm5pXPI/user/rpn_calc:
total 48
-rwxr-xr-x  1 deepthought  deepthought  1138 Jan 16 10:44 computer.c
-rwxr-xr-x  1 deepthought  deepthought  1128 Jan 16 10:44 ops.c
-rwxr-xr-x  1 deepthought  deepthought  1045 Jan 16 10:44 ops.h
-rwxr-xr-x  1 deepthought  deepthought  2303 Jan 16 10:44 rpn_calc.c
-rwxr-xr-x  1 deepthought  deepthought  1126 Jan 16 10:44 rpn_calc.h
-rwxr-xr-x  1 deepthought  deepthought  1356 Jan 16 10:44 utils.c
 
= rpn_calc =====================================================================
$> clang -Wextra -Wall -Werror ops.c rpn_calc.c utils.c computer.c -o user_exe 

= Test 1 ===================================================
$> ./aesv6gb2qj0aoyg2npq6axje 
$> diff -U 3 user_output_test1 test1.output | cat -e

Diff OK :D
= Test 2 ===================================================
$> ./n4dmsafnbu5j61rv5wr0hh2s "2 3 -" "5 6 +"
$> diff -U 3 user_output_test2 test2.output | cat -e

Diff OK :D
= Test 3 ===================================================
$> ./u27n2wy3f2rqbc59xgimrvle "2 -"
$> diff -U 3 user_output_test3 test3.output | cat -e

Diff OK :D
= Test 4 ===================================================
$> ./7kejiwhy0zlkl52m5kp40mha "-"
$> diff -U 3 user_output_test4 test4.output | cat -e

Diff OK :D
= Test 5 ===================================================
$> ./okwhd39f0qm80jaia8abmo8x "2 5 6 -"
$> diff -U 3 user_output_test5 test5.output | cat -e

Diff OK :D
= Test 6 ===================================================
$> ./930eepwyw4kulu349ldrjknu "3 4 +"
$> diff -U 3 user_output_test6 test6.output | cat -e

Diff OK :D
= Test 7 ===================================================
$> ./h1o2m6920ka894z00zsxsidt "2 4 + 8 *"
$> diff -U 3 user_output_test7 test7.output | cat -e

Diff OK :D
= Test 8 ===================================================
$> ./ayp5ptpgd1bm0kilg8tldl09 "50 10 25 + -"
$> diff -U 3 user_output_test8 test8.output | cat -e

Diff OK :D
= Test 9 ===================================================
$> ./hwnbmatpkd5p5f9y6r1an48j "1000 5 2 * /"
$> diff -U 3 user_output_test9 test9.output | cat -e

Diff OK :D
= Test 10 ==================================================
$> ./svore3cpnnsmv38aqrlz9fz5 "1000 3 %"
$> diff -U 3 user_output_test10 test10.output | cat -e

Diff OK :D
= Test 11 ==================================================
$> ./17s3d8aj6963iw31vgyvuu82 "1000 0 /"
$> diff -U 3 user_output_test11 test11.output | cat -e

Diff OK :D
= Test 12 ==================================================
$> ./nnfzaga4umjt7gzkj7ahoo9a "1000 0 %"
Command './nnfzaga4umjt7gzkj7ahoo9a "1000 0 %"' got killed by a Floating exception (Signal -8)
Grade: 0

= Final grade: 0 ===============================================================

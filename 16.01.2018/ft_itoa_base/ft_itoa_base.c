/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itoa_base.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: exam <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/16 10:05:48 by exam              #+#    #+#             */
/*   Updated: 2018/01/16 10:25:25 by exam             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>

int		char_amount(int value, int base, char **allocate)
{
	int	counter;

	counter = 0;
	if (value < 0 && base == 10)
		counter++;
	while (value / base)
	{
		counter++;
		value /= base;
	}
	*allocate = malloc((counter + 2) * sizeof(char));
	if (*allocate)
	{
		(*allocate)[counter] = 0;
		if (value < 0 && base == 10)
			(*allocate)[0] = '-';
	}
	return (counter);
}

char	get_number(int n)
{
	if (n >= 0 && n <= 9)
		return (n + '0');
	if (n >= 10 && n <= 15)
		return (n + 55);
	return (0);
}

char	*ft_itoa_base(int value, int base)
{
	int		size;
	char	*result;
	long	long_value;

	long_value = value;
	size = char_amount(value, base, &result);
	if (!result)
		return (0);
	if (value < 0 && base == 10)
		long_value *= -1;
	while (long_value / base)
	{
		result[size] = get_number(long_value % base);
		size--;
		long_value /= base;
	}
	result[size] = get_number(long_value % base);
	return (result);
}

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_rrange.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aivanov <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/13 19:06:02 by aivanov           #+#    #+#             */
/*   Updated: 2017/11/13 19:25:44 by aivanov          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>

int	*ft_rrange(int start, int end)
{
	int	amount;
	int	*result;
	int	i;

	i = -1;
	if (start > end)
	{
		amount = start - end + 1;
		if ((result = malloc(amount * sizeof(int))) == NULL)
			return (NULL);
		while (end + ++i <= start)
			result[i] = end + i;
		return (result);
	}
	amount = end - start + 1;
	if ((result = malloc(amount * sizeof(int))) == NULL)
		return (NULL);
	while (end >= start + ++i)
		result[i] = end - i;
	return (result);
}

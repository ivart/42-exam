/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   brackets.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: exam <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/02 10:50:41 by exam              #+#    #+#             */
/*   Updated: 2018/01/02 10:54:07 by exam             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "brackets.h"

void	remove_other_symbols(char *str)
{
	int	i;
	int	j;

	i = 0;
	j = 0;
	while (str[i])
	{
		if (is_bracket(str[i]))
			str[j++] = str[i];
		i++;
	}
	str[j] = 0;
}

_Bool	is_string_valid(char *str)
{
	int	i;

	remove_other_symbols(str);
	i = 0;
	while (str[i])
	{
		if (str[i] == ')' || str[i] == ']' || str[i] == '}')
		{
			if (i != 0 && is_same_bracket(str[i - 1], str[i]))
			{
				move(str + i - 1, str + i + 1);
				i -= 2;
			}
			else
				return (0);
		}
		i++;
	}
	if (!str[0])
		return (1);
	return (0);
}

int		main(int argc, char **argv)
{
	int	i;

	if (argc == 1)
	{
		write(1, "\n", 1);
		return (0);
	}
	i = 1;
	while (i < argc)
	{
		VALIDATE(is_string_valid(argv[i]));
		i++;
	}
	return (0);
}

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   brackets.h                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: exam <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/02 10:54:36 by exam              #+#    #+#             */
/*   Updated: 2018/01/02 10:54:42 by exam             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef BRACKETS_H
# define BRACKETS_H
# include <unistd.h>
# define VALIDATE(x) if (x) write(1, "OK\n", 3); else write(1, "Error\n", 6)

typedef struct	s_bracket_list
{
	char	braket;
	void	*prev;
	void	*next;
}				t_bracket_list;
_Bool			is_bracket(char c);
_Bool			is_same_bracket(char a, char b);
void			move(char *dst, char *src);
#endif

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fprime.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aivanov <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/13 20:03:24 by aivanov           #+#    #+#             */
/*   Updated: 2017/11/13 20:29:28 by aivanov          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <stdio.h>

unsigned int	gfpd(unsigned int nb, unsigned int start)
{
	unsigned int	i;

	if (nb == 4)
		return (2);
	i = start;
	while (i <= nb / i)
	{
		if (nb % i == 0)
			return (i);
		i++;
	}
	return (nb);
}

int				main(int argc, char **argv)
{
	_Bool			is_first;
	unsigned int	inp;
	unsigned int	divider;

	if (argc == 2)
	{
		is_first = 1;
		divider = 2;
		inp = atoi(argv[1]);
		if (inp == 1)
			printf("1");
		while (inp != 1)
		{
			divider = gfpd(inp, divider);
			if (!is_first)
				printf("*");
			else
				is_first = 0;
			printf("%d", divider);
			inp /= divider;
		}
	}
	printf("\n");
	return (0);
}

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_range.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: exam <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/05 10:28:35 by exam              #+#    #+#             */
/*   Updated: 2017/12/05 10:43:00 by exam             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>

int	*ft_range(int start, int end)
{
	int	len;
	int	step;
	int	i;
	int	*result;

	len = start - end + 1;
	step = -1;
	if (start < end)
	{
		len = end - start + 1;
		step = 1;
	}
	if ((result = malloc(len * sizeof(int))) == 0)
		return (0);
	i = 0;
	while (start + i * step != end)
	{
		result[i] = start + i * step;
		i++;
	}
	result[i] = end;
	return (result);
}

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   alpha_mirror.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aivanov <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/13 18:47:22 by aivanov           #+#    #+#             */
/*   Updated: 2017/11/13 19:03:21 by aivanov          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

int	main(int argc, char **argv)
{
	int		i;
	char	character;

	if (argc == 2)
	{
		i = 0;
		while (argv[1][i])
		{
			character = argv[1][i];
			if ('a' <= argv[1][i] && argv[1][i] <= 'z')
				character = 'z' - argv[1][i] + 'a';
			else if ('A' <= argv[1][i] && argv[1][i] <= 'Z')
				character = 'Z' - argv[1][i] + 'A';
			write(1, &character, 1);
			i++;
		}
	}
	write(1, "\n", 1);
	return (0);
}

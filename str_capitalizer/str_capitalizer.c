/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   str_capitalizer.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aivanov <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/13 19:34:41 by aivanov           #+#    #+#             */
/*   Updated: 2017/11/13 20:00:06 by aivanov          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

_Bool	is_upper(char c)
{
	return ('A' <= c && 'Z' >= c);
}

_Bool	is_lower(char c)
{
	return ('a' <= c && c <= 'z');
}

_Bool	ft_is_delimeter(char c)
{
	return (c == ' ' || c == '\t' || c == '\n');
}

int		main(int argc, char **argv)
{
	int		i;
	int		j;
	char	character;

	i = 1;
	while (i < argc)
	{
		j = -1;
		while (argv[i][++j])
		{
			character = argv[i][j];
			if (is_lower(argv[i][j]) && (j == 0
						|| ft_is_delimeter(argv[i][j - 1])))
				character += ('A' - 'a');
			else if (is_upper(argv[i][j]) && j != 0
					&& !ft_is_delimeter(argv[i][j - 1]))
				character -= ('A' - 'a');
			write(1, &character, 1);
		}
		if (++i < argc)
			write(1, "\n", 1);
	}
	write(1, "\n", 1);
	return (0);
}

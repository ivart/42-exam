/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_list_size.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: exam <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/26 10:42:01 by exam              #+#    #+#             */
/*   Updated: 2017/12/26 10:44:00 by exam             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_list.h"

int	ft_list_size(t_list *begin_list)
{
	int	result;

	if (!begin_list)
		return (0);
	result = 1;
	while (begin_list->next)
	{
		begin_list = begin_list->next;
		result++;
	}
	return (result);
}

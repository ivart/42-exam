/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print_memory.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: exam <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/16 10:46:27 by exam              #+#    #+#             */
/*   Updated: 2018/01/16 11:36:14 by exam             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

void	write_digit(unsigned char q)
{
	char	tmp;

	if (q >= 0 && q <= 9)
		tmp = q + '0';
	else
		tmp = q + 'a' - 10;
	write(1, &tmp, 1);
}

void	write_letter(unsigned char q)
{
	if (q >= 0x20 && q <= 0x7e)
		write(1, &q, 1);
	else
		write(1, ".", 1);
}

void	print_bytes(const void *addr, size_t size)
{
	unsigned char		i;

	i = 0;
	while (i < 16 && i < size)
	{
		write_digit(((unsigned char *)addr)[i] / 16);
		write_digit(((unsigned char *)addr)[i] % 16);
		if (i % 2)
			write(1, " ", 1);
		i++;
	}
	while (i < 16)
	{
		write(1, "  ", 2);
		if (i % 2)
			write(1, " ", 1);
		i++;
	}
}

void	print_letters(const void *addr, size_t size)
{
	unsigned char		i;

	i = 0;
	while (i < 16 && i < size)
	{
		write_letter(((unsigned char *)addr)[i]);
		i++;
	}
	write(1, "\n", 1);
}

void	print_memory(const void *addr, size_t size)
{
	size_t	i;

	i = 0;
	while (i < size)
	{
		print_bytes((unsigned char *)addr + i, size - i);
		print_letters((unsigned char *)addr + i, size - i);
		i += 16;
	}
}

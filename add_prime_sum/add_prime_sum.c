/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   add_prime_sum.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: exam <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/02 10:03:06 by exam              #+#    #+#             */
/*   Updated: 2018/01/02 10:45:19 by exam             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

unsigned int	get_next_prime(unsigned int start, unsigned int stop)
{
	unsigned int	i;

	if (start < 2)
		start = 2;
	while (start <= stop)
	{
		i = 2;
		while (i <= start / i)
		{
			if (start % i == 0)
				break ;
			i++;
		}
		if (i > start / i)
			return (start);
		start++;
	}
	return (0);
}

int				ft_atoi(int argc, char **argv)
{
	int	result;
	int	i;

	if (argc != 2)
		return (0);
	i = 0;
	result = 0;
	while (argv[1][i])
	{
		if (argv[1][i] < '0' || argv[1][i] > '9')
			return (0);
		result *= 10;
		result += argv[1][i] - '0';
		i++;
	}
	return (result);
}

void			ft_putnbr(unsigned long long nbr)
{
	char	c;

	if (nbr < 10)
	{
		c = '0' + nbr;
		write(1, &c, 1);
	}
	else
	{
		c = '0' + nbr % 10;
		ft_putnbr(nbr / 10);
		write(1, &c, 1);
	}
}

int				main(int argc, char **argv)
{
	unsigned long long	result;
	unsigned int		next_prime;
	int					target;

	result = 0;
	target = ft_atoi(argc, argv);
	next_prime = get_next_prime(2, target);
	result += next_prime;
	while (next_prime != 0)
	{
		next_prime = get_next_prime(next_prime + 1, target);
		result += next_prime;
	}
	ft_putnbr(result);
	write(1, "\n", 1);
	return (0);
}

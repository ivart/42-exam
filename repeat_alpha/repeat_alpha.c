/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   repeat_alpha.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aivanov <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/13 17:52:48 by aivanov           #+#    #+#             */
/*   Updated: 2017/11/13 18:06:10 by aivanov          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

int	main(int argc, char **argv)
{
	int	i;
	int	j;

	if (argc == 2)
	{
		i = 0;
		while (argv[1][i] != 0)
		{
			if ('a' < argv[1][i] && argv[1][i] <= 'z')
			{
				j = argv[1][i] - 'a';
				while (j-- > 0)
					write(1, &argv[1][i], 1);
			}
			else if ('A' < argv[1][i] && argv[1][i] <= 'Z')
			{
				j = argv[1][i] - 'A';
				while (j-- > 0)
					write(1, &argv[1][i], 1);
			}
			write(1, &argv[1][i++], 1);
		}
	}
	write(1, "\n", 1);
	return (0);
}
